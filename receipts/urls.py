from django.urls import path
from receipts.views import receipt_list

# redirects localhost:8000/ to localhost:8000/receipts
urlpatterns = [path("", receipt_list, name="home")]
