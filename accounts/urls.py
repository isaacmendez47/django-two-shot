from django.urls import path
from accounts.views import user_login, user_logout

urlpatterns = [
    path("logout/", user_logout, name="logout"),  # <--feature 9.2
    path("login/", user_login, name="login"),
]
